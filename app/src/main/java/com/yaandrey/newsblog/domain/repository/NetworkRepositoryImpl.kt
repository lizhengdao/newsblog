package com.yaandrey.newsblog.domain.repository

import com.yaandrey.newsblog.DATE_FROM
import com.yaandrey.newsblog.PLATFORM
import com.yaandrey.newsblog.SORTED
import com.yaandrey.newsblog.api.Api
import com.yaandrey.newsblog.api.dto.NewsDto
import com.yaandrey.newsblog.domain.repository.network.NetworkRepository
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor (private val api: Api) :
    NetworkRepository {

    override fun getNewsPageOne(page: Int): NewsDto? {
        return api.getNews(PLATFORM, DATE_FROM, SORTED,page)
            .execute()
            .body()
    }

//    override fun getNewsResultStream(page: Int): Flow<PagingData<NewsModel>> {
//        return Pager(
//            config = PagingConfig(
//                pageSize = 20,
//                prefetchDistance = 5,
//                enablePlaceholders = true,
//                initialLoadSize = 5
//            ), remoteMediator = NewsRemoteMediator(this),
//            pagingSourceFactory =  { NewsPagingSource(this) }
//        ).flow
//    }

}