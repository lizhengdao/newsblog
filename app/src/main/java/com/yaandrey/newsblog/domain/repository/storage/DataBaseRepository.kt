package com.yaandrey.newsblog.domain.repository.storage

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.yaandrey.newsblog.storage.AppDataBase
import com.yaandrey.newsblog.ui.domain.model.NewsModel
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import com.yaandrey.newsblog.ui.storage.entity.RemoteKeysEntity

interface DataBaseRepository {
    suspend fun insert(list: List<NewsEntity>)
    fun getNews(): PagingSource<Int, NewsEntity>
    fun getAppDataBase(): AppDataBase
}