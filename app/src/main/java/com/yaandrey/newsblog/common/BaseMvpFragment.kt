package com.yaandrey.newsblog.common

import android.util.Log
import com.google.android.material.snackbar.Snackbar
import moxy.MvpAppCompatFragment

abstract class BaseMvpFragment : MvpAppCompatFragment(), BaseMvpView {

    override fun showDialog(title: String, message: String) {
        Log.e("ERROR_LOG", "Error: $message")
        val view = view ?: return
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }
}