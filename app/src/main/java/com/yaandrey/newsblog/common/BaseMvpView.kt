package com.yaandrey.newsblog.common

import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface BaseMvpView : MvpView {

    fun showDialog(title: String, message: String)
}