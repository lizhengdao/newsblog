package com.yaandrey.newsblog.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import moxy.MvpPresenter

abstract class BaseMvpPresenter<mainView : BaseMvpView>
  : MvpPresenter<mainView>(), CoroutineScope by MainScope() {

}