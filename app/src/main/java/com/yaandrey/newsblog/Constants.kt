package com.yaandrey.newsblog

const val BASE_URL = "https://newsapi.org/v2/?q=android&from=2019-04-00&sortBy=publishedAt&apiKey=26eddb253e7840f988aec61f2ece2907"
const val PLATFORM ="android"
const val DATE_FROM = "2020-04-00"
const val SORTED = "publishedAt"
const val STARTING_PAGE_INDEX = 1