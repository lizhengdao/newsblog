package com.yaandrey.newsblog.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yaandrey.newsblog.ui.storage.dao.NewsDao
import com.yaandrey.newsblog.ui.storage.dao.RemoteKeysDao
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import com.yaandrey.newsblog.ui.storage.entity.RemoteKeysEntity

@Database(version = 7, exportSchema = false, entities = [NewsEntity::class, RemoteKeysEntity::class])
abstract class AppDataBase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun remoteKeysDao(): RemoteKeysDao
}