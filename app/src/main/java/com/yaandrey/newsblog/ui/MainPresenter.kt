package com.yaandrey.newsblog.ui

import androidx.paging.*
import com.yaandrey.newsblog.common.BaseMvpPresenter
import com.yaandrey.newsblog.domain.repository.network.NetworkRepository
import com.yaandrey.newsblog.domain.repository.storage.DataBaseRepository
import com.yaandrey.newsblog.ui.data.datasource.NewsRemoteMediator
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import kotlinx.coroutines.flow.Flow
import moxy.InjectViewState
import javax.inject.Inject

@ExperimentalPagingApi
@InjectViewState
class MainPresenter
@Inject constructor(
    private val network: NetworkRepository,
    private val db: DataBaseRepository
) : BaseMvpPresenter<MainView>() {

    private var currentNews: Flow<PagingData<NewsEntity>>? = null


    fun loadNews(): Flow<PagingData<NewsEntity>> {
        val lastResult = currentNews
        if (lastResult != null) return lastResult
        val result = getNewsResultStream()
            .cachedIn(this)
        currentNews = result
        return result
    }

    private fun getNewsResultStream(): Flow<PagingData<NewsEntity>> {
        val pagingSourceFactory = { db.getNews() }
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                prefetchDistance = 5
            ),
            remoteMediator = NewsRemoteMediator(network, db.getAppDataBase()),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }
}