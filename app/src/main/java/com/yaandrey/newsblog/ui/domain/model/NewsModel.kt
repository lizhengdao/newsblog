package com.yaandrey.newsblog.ui.domain.model

import com.yaandrey.newsblog.domain.model.MainModel

data class NewsModel(
    val uid: String,
    val title: String,
    val description: String,
    val imageUrl: String,
    val url: String,
    val publicationDate: String
) : MainModel()