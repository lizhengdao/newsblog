package com.yaandrey.newsblog.ui.data.datasource

import androidx.paging.PagingSource
import com.yaandrey.newsblog.domain.repository.network.NetworkRepository
import com.yaandrey.newsblog.ui.data.ToListModelMapper
import com.yaandrey.newsblog.ui.domain.model.NewsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

/** Testing not using  */

class NewsPagingSource(
    private val networkRepository: NetworkRepository
) : PagingSource<Int, NewsModel>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsModel> {
        val position = params.key ?: 1
        return try {
            val response = withContext(Dispatchers.IO) { networkRepository.getNewsPageOne(position) } ?: return LoadResult.Error(NullPointerException("Response is null"))
            val result = ToListModelMapper().map(response)
            LoadResult.Page(
                data = result,
                prevKey = if (position == 1) null else position - 1,
                nextKey = if (result.isEmpty()) null else position + 1
            )
        } catch (error: IOException) {
            return LoadResult.Error(error)
        } catch (error: HttpException) {
            return LoadResult.Error(error)
        }
    }
}