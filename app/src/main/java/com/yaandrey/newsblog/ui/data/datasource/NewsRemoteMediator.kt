package com.yaandrey.newsblog.ui.data.datasource

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.yaandrey.newsblog.STARTING_PAGE_INDEX
import com.yaandrey.newsblog.domain.repository.network.NetworkRepository
import com.yaandrey.newsblog.storage.AppDataBase
import com.yaandrey.newsblog.ui.data.ToListEntityFromModelMapper
import com.yaandrey.newsblog.ui.data.ToListModelMapper
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import com.yaandrey.newsblog.ui.storage.entity.RemoteKeysEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

@OptIn(ExperimentalPagingApi::class)
class NewsRemoteMediator(
    private val network: NetworkRepository,
    private val db: AppDataBase
) : RemoteMediator<Int, NewsEntity>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, NewsEntity>): MediatorResult {

        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                    ?: throw InvalidObjectException("Remote key should not be null for $loadType")
                remoteKeys.prevKey ?: return MediatorResult.Success(endOfPaginationReached = true)
                remoteKeys.prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys?.nextKey == null) throw InvalidObjectException("Remote key should not be null for $loadType")
                remoteKeys.nextKey
            }
        }

        try {
            val apiResponse = withContext(Dispatchers.IO) { network.getNewsPageOne(page) }
                ?: return MediatorResult.Error(NullPointerException("Response is null"))

            val news = ToListModelMapper().map(apiResponse)
            val newsEntity = ToListEntityFromModelMapper().map(news)
            val endOfPaginationReached = news.isEmpty()
            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    db.newsDao().clearNews()
                    db.remoteKeysDao().cleatRemoteKeys()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = news.map {
                    RemoteKeysEntity(
                        uid = it.uid,
                        prevKey = prevKey,
                        nextKey = nextKey
                    )
                }
                db.remoteKeysDao().insertAll(keys)
                db.newsDao().insertNews(newsEntity)
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (error: IOException) {
            return MediatorResult.Error(error)
        } catch (error: HttpException) {
            return MediatorResult.Error(error)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, NewsEntity>): RemoteKeysEntity? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { news -> db.remoteKeysDao().remoteKeysNewsId(news.uid) }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, NewsEntity>): RemoteKeysEntity? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { news -> db.remoteKeysDao().remoteKeysNewsId(news.uid) }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, NewsEntity>): RemoteKeysEntity? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.uid?.let {
                db.remoteKeysDao().remoteKeysNewsId(it)
            }
        }
    }
}