package com.yaandrey.newsblog.ui.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.yaandrey.newsblog.ui.domain.model.NewsModel

class NewsAdapter(private val clickListener: OnClickItemListener) :
    PagingDataAdapter<NewsModel, NewsViewHolder>(NEWS_COMPARATOR) {

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val newsItem = getItem(position)
        if (newsItem != null) holder.onBind(newsItem, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NewsViewHolder.create(parent)

    interface OnClickItemListener {
        fun onClickItem(item: NewsModel)
        fun retry()
    }

    companion object {
        private val NEWS_COMPARATOR = object : DiffUtil.ItemCallback<NewsModel>() {
            override fun areItemsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean {
                return oldItem.url == newItem.url
            }

            override fun areContentsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean = oldItem == newItem
        }
    }
}