package com.yaandrey.newsblog.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yaandrey.newsblog.R
import com.yaandrey.newsblog.ui.domain.model.NewsModel
import kotlinx.android.synthetic.main.item_news.view.*

class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val title = view.title
    private val description = view.description
    private val publicationDate = view.publication_date
    private val imageNews = view.image_news

    private var newsModel: NewsModel? = null

    fun onBind(item: NewsModel, clickListener: NewsAdapter.OnClickItemListener) {
        this.newsModel = item
        title.text = item.title
        description.text = item.description
        publicationDate.text = item.publicationDate
        Glide.with(itemView)
            .load(item.imageUrl)
            .into(imageNews)
            .waitForLayout()
        itemView.setOnClickListener { clickListener.onClickItem(item) }
    }

    companion object {
        fun create(
            parent: ViewGroup
        ): NewsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.item_news, parent, false)
            return NewsViewHolder(view)
        }
    }
}