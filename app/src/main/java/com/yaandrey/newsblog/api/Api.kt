package com.yaandrey.newsblog.api

import com.yaandrey.newsblog.api.dto.NewsDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("everything")
    fun getNews(
        @Query("q") platform: String,
        @Query("from") dateFrom: String,
        @Query("sortBy") sorted: String,
        @Query("page") numPage: Int
    ): Call<NewsDto>
}