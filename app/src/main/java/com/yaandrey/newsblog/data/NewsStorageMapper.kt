package com.yaandrey.newsblog.data

abstract class Mapper<From, To> {
    abstract fun map(from: From): To
}