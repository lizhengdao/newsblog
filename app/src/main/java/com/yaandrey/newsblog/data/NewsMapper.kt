package com.yaandrey.newsblog.data

import com.yaandrey.newsblog.api.dto.NewsDto

abstract class NewsMapper<To> {
    abstract fun map(from: NewsDto): List<To>
}

abstract class NewsToListModel<From, To> {
    abstract fun map(from: List<From>): List<To>
}