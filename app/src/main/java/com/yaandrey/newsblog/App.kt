package com.yaandrey.newsblog

import android.app.Application
import com.facebook.stetho.Stetho
import com.yaandrey.newsblog.di.AppComponent
import com.yaandrey.newsblog.di.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        INSTANSE = this
        appComponent = DaggerAppComponent
            .builder()
            .context(this)
            .build()
        if (BuildConfig.DEBUG) {
            initMonitoring()
        }
    }

    companion object {
        private lateinit var INSTANSE: App
        @JvmStatic
        fun get(): App = INSTANSE
    }

    private fun initMonitoring() {
        Stetho.initialize(
            Stetho.newInitializerBuilder(this)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .build()
        )
    }
}